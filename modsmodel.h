#ifndef MODSMODEL_H
#define MODSMODEL_H

#include <QAbstractListModel>
#include <QVector>

struct ModItem
{
    QString label;
    bool checked;
    QString path;
};

class ModsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ModsModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    void populate();

    QVector<ModItem> list() const;
    QStringList checkedItems() const;
    QString path(const QModelIndex &index) const;

private:
    QVector<ModItem> m_items;
};

#endif // MODSMODEL_H
