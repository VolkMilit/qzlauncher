#ifndef PROCESSINVOKER_H
#define PROCESSINVOKER_H

#include <QProcess>

class ProcessInvoker : public QProcess
{
    Q_OBJECT

public:
    ProcessInvoker(QObject *parent);

    enum Errors
    {
        NoError = 0,
        NoFileError
    };

    void setEnginePath(const QString &enginePath);
    void setGame(const QString &game);
    void setMods(const QStringList &mods);
    void gameArgument(const QString &gameArgument);
    void modArgument(const QString &modArgument);

    int run();

    QByteArray outputString() const;

signals:
    void output(const QByteArray &out);

private:
    QString m_enginePath;
    QStringList m_arguments;

    QString m_gameArgument = QStringLiteral("-IWAD");
    QString m_modArgument = QStringLiteral("-file");

    QByteArray m_outputString;
};

#endif // PROCESSINVOKER_H
