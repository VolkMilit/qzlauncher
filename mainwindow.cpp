#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "processinvoker.h"
#include "settingsdialog.h"
#include "pk3handler.h"
#include "wadhandler.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);

    m_gamesModel.populate();
    ui->gameListView->setModel(&m_gamesModel);

    m_modsModel.populate();
    ui->modsListView->setModel(&m_modsModel);

    connect(ui->playButton, &QPushButton::clicked, this, &MainWindow::play);
    connect(ui->actionSettings, &QAction::triggered, [=](){SettingsDialog().exec();});
    connect(ui->modsListView, &QListView::clicked, this, &MainWindow::setDescritption);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::play()
{
    //ui->stackedWidget->setCurrentIndex(1);

    const QModelIndex gameIndex = ui->gameListView->selectionModel()->currentIndex();

    if (!gameIndex.isValid())
    {
        QMessageBox::warning(this, tr("QZLauncher - Error!"), tr("Select game first!"));
        return;
    }

    ProcessInvoker invoker(this);
    invoker.setEnginePath(m_settings.defaultEnginePath());
    invoker.setGame(m_gamesModel.path(gameIndex));
    invoker.setMods(m_modsModel.checkedItems());

    /*connect(&invoker, &ProcessInvoker::readyReadStandardOutput, [&](){
        while (invoker.canReadLine())
            ui->outputTextBrowser->setText(invoker.readLine());
    });*/

    //connect(&invoker, &ProcessInvoker::output, ui->outputTextBrowser, &QTextBrowser::append);

    if (invoker.run() == ProcessInvoker::NoFileError)
        QMessageBox::warning(this, tr("QZLauncher - Error!"), tr("Can't find engine executable!"));
}

void MainWindow::setDescritption(const QModelIndex &index)
{
    Pk3Handler handler(m_modsModel.path(index));

    if (handler.isMarkdown())
        ui->descriptionTextEdit->setMarkdown(handler.description());
    else
        ui->descriptionTextEdit->setText(handler.description());
}
