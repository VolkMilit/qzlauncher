#include "modsmodel.h"

#include <QDirIterator>
#include <QFileInfo>
#include <QDebug>

ModsModel::ModsModel(QObject *parent)
    : QAbstractListModel(parent)
{}

int ModsModel::rowCount(const QModelIndex &parent) const
{
    return m_items.length();
}

QVariant ModsModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
        return m_items[index.row()].label;
    else if (role == Qt::CheckStateRole)
        return m_items[index.row()].checked;

    return QVariant();
}

bool ModsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::CheckStateRole)
    {
        m_items[index.row()].checked = !m_items[index.row()].checked;
        return true;
    }

    return false;
}

Qt::ItemFlags ModsModel::flags(const QModelIndex &index) const
{
    return QAbstractListModel::flags(index) | Qt::ItemIsUserCheckable;
}

void ModsModel::populate()
{
    QDirIterator it("/media/drive1/games/doom-old/pwad/", {"*.wad", "*.pk3", "*.zip"}, QDir::Files);

    while (it.hasNext())
    {
        it.next();

        ModItem item;
        item.label = QFileInfo(it.filePath()).baseName();
        item.checked = false;
        item.path = it.filePath();

        m_items.push_back(item);
    }
}

QVector<ModItem> ModsModel::list() const
{
    return m_items;
}

QStringList ModsModel::checkedItems() const
{
    QStringList ret;

    foreach (const auto &item, m_items)
        if (item.checked)
            ret.push_back(item.path);

    return ret;
}

QString ModsModel::path(const QModelIndex &index) const
{
    return m_items[index.row()].path;
}
