#include "processinvoker.h"

#include <QFileInfo>
#include <QTextStream>

#include <QDebug>

ProcessInvoker::ProcessInvoker(QObject *parent) :
    QProcess(parent)
{
    setProcessChannelMode(QProcess::ForwardedChannels);

    /*connect(this, &QProcess::readyReadStandardOutput, [this]() {
        while(canReadLine())
            emit output(readLine());
    });*/
}

void ProcessInvoker::setEnginePath(const QString &enginePath)
{
    m_enginePath = enginePath;
}

void ProcessInvoker::setGame(const QString &game)
{
    m_arguments.push_back(m_gameArgument);
    m_arguments.push_back(game);
}

void ProcessInvoker::setMods(const QStringList &mods)
{
    foreach (const auto &item, mods)
    {
        m_arguments.push_back(m_modArgument);
        m_arguments.push_back(item);
    }
}

void ProcessInvoker::gameArgument(const QString &gameArgument)
{
    m_gameArgument = gameArgument;
}

void ProcessInvoker::modArgument(const QString &modArgument)
{
    m_modArgument = modArgument;
}

int ProcessInvoker::run()
{
    if (!QFileInfo::exists(m_enginePath))
        return NoFileError;

    startDetached(m_enginePath, m_arguments);
    return NoError;
}

QByteArray ProcessInvoker::outputString() const
{
    return m_outputString;
}
