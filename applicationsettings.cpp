#include "applicationsettings.h"

#include <QFileInfo>

ApplicationSettings::ApplicationSettings() :
    QSettings()
{}

void ApplicationSettings::setEnginePath(const QString &path)
{
}

QString ApplicationSettings::enginePath() const
{
}

QString ApplicationSettings::defaultEnginePath() const
{
    const QStringList binPaths = qEnvironmentVariable("PATH").split(":");

    foreach (const auto &item, binPaths)
    {
        QFileInfo info(item + "/gzdoom");

        if (info.exists() && info.isExecutable())
            return item + "/gzdoom";
    }

    return QString();
}
