#ifndef GAMESMODEL_H
#define GAMESMODEL_H

#include <QAbstractListModel>

struct GameItem
{
    QString label;
    bool checked;
    QString path;
};

class GamesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit GamesModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void populate();

    QVector<GameItem> list() const;
    QString path(const QModelIndex &index) const;

private:
    QVector<GameItem> m_games;
};

#endif // GAMESMODEL_H
