#include "gamesmodel.h"

#include <QDirIterator>
#include <QFileInfo>

GamesModel::GamesModel(QObject *parent) :
    QAbstractListModel(parent)
{}

int GamesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_games.length();
}

QVariant GamesModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
        return m_games[index.row()].label;

    return QVariant();
}

void GamesModel::populate()
{
    QDirIterator it("/media/drive1/games/doom-old/iwad/", {"*.wad", "*.pk3", "*.zip"}, QDir::Files);

    while (it.hasNext())
    {
        it.next();

        GameItem item;
        item.label = QFileInfo(it.filePath()).baseName();
        item.checked = false;
        item.path = it.filePath();

        m_games.push_back(item);
    }
}

QVector<GameItem> GamesModel::list() const
{
    return m_games;
}

QString GamesModel::path(const QModelIndex &index) const
{
    return m_games[index.row()].path;
}
