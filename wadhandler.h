#ifndef WADHANDLER_H
#define WADHANDLER_H

#include <cstdint>

#include <QString>

struct header_t
{
    char     identifier[4];
    uint32_t items;
    uint32_t directory;
};

struct directory_t
{
    uint32_t start;
    uint32_t size;
    char     lumpname[8];
};

class WadHandler
{
public:
    WadHandler(const QString &file);

private:
    void parse();

    QString m_file;
};

#endif // WADHANDLER_H
