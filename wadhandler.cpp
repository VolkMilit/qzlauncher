#include "wadhandler.h"

#include <cstddef>
#include <cstdlib>
#include <QFile>
#include <QDir>
#include <QDebug>

WadHandler::WadHandler(const QString &file) :
    m_file(file)
{
    parse();
}

void WadHandler::parse()
{
    size_t l;

    QFile wadfile(m_file);

    if (wadfile.open(QIODevice::ReadOnly))
    {
        struct header_t header;

        l = wadfile.read(reinterpret_cast<char *>(&header), sizeof(struct header_t));

        if (wadfile.error() || l < 1)
        {
            wadfile.close();
            qDebug() << "! Header";
            return;
        }

        switch(*(uint32_t*)header.identifier)
        {
            case 0x44415749: /* IWAD */
                break;
            case 0x44415750: /* PWAD */
                break;
            default: // Unknown header
                wadfile.close();
                qDebug() << "! Unknown header";
                return;
        }

        const QString path = "/tmp/123/";
        QDir().mkdir(path);

        struct directory_t *directory = new directory_t[header.items * sizeof(struct directory_t)];
        wadfile.seek(header.directory);

        l = wadfile.read(reinterpret_cast<char*>(&directory), sizeof(struct directory_t));

        if (wadfile.error() || l < header.items)
        {
            wadfile.close();
            qDebug() << "! Directory";
            return;
        }

        for (int i = 0; i < header.items; ++i)
        {
            char sane_lumpname[9], lumpname[9] = "\0\0\0\0\0\0\0\0";
            snprintf(lumpname, 9, "%s", directory[i].lumpname);

            for (l = 0; l < 9; ++l)
            {
                char c = lumpname[l];
                switch(c)
                {
                    case '\\':
                    case '[':
                    case ']':
                        c = '_';
                    default:
                        sane_lumpname[l] = c;
                }
            }

            QFile lumpfile(QString("%1/%2_%3.lmp").arg(path, QString::number(i+1), sane_lumpname));
            lumpfile.open(QIODevice::WriteOnly);

            if (directory[i].size > 0)
            {
                char *lumpdata = new char;
                wadfile.seek(directory[i].start);
                l = wadfile.read(lumpdata, directory[i].size);

                if (wadfile.error() || l < directory[i].size)
                {
                    wadfile.close();
                    return;
                }

                wadfile.write(lumpdata, directory[i].size);
                delete lumpdata;
            }

            lumpfile.close();
        }

        delete[] directory;
        wadfile.close();
    }
}
