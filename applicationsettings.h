#ifndef APPLICATIONSETTINGS_H
#define APPLICATIONSETTINGS_H

#include <QSettings>

class ApplicationSettings : public QSettings
{
public:
    explicit ApplicationSettings();

    void setEnginePath(const QString &path);
    QString defaultEnginePath() const;
    QString enginePath() const;
};

#endif // APPLICATIONSETTINGS_H
