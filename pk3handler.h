#ifndef PK3HANDLER_H
#define PK3HANDLER_H

#include <QString>

class Pk3Handler
{
public:
    Pk3Handler(const QString &file);

    QString name() const;
    QString description() const;
    bool isMarkdown() const;

private:
    QString m_name;
    QString m_description;
    bool m_isMarkdown;
};

#endif // PK3HANDLER_H
