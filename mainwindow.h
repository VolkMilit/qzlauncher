#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>

#include "gamesmodel.h"
#include "modsmodel.h"
#include "applicationsettings.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void play();

private slots:
    void setDescritption(const QModelIndex &index);

private:
    Ui::MainWindow *ui;

    GamesModel m_gamesModel{this};
    ModsModel m_modsModel{this};
    ApplicationSettings m_settings;
};
#endif // MAINWINDOW_H
