#include "pk3handler.h"

#include <quazip/quazip.h>
#include <quazip/quazipfile.h>

#include <QFileInfo>

Pk3Handler::Pk3Handler(const QString &file)
{
    m_name = QFileInfo(file).baseName();
    m_description = QStringLiteral("This wad has no description.");

    QuaZip pk3(file);

    if (pk3.open(QuaZip::mdUnzip))
    {
        for (bool file = pk3.goToFirstFile(); file; file = pk3.goToNextFile())
        {
            const QString filePath = pk3.getCurrentFileName();

            if (filePath.contains(QStringLiteral("gameinfo"), Qt::CaseInsensitive))
            {
                QuaZipFile zipFile(&pk3);
                zipFile.setFileName(filePath);

                if (zipFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    while (!zipFile.atEnd())
                    {
                        const QString line = zipFile.readLine();

                        if (line.contains(QStringLiteral("StartupTitle"), Qt::CaseInsensitive))
                            m_name = line.split('=')[1].trimmed().replace('"', "");
                    }
                }
            }

            if (filePath.contains(QStringLiteral("readme"), Qt::CaseInsensitive))
            {
                if (QFileInfo(filePath).suffix().toLower() == "md")
                    m_isMarkdown = true;

                QuaZipFile zipFile(&pk3);
                zipFile.setFileName(filePath);

                if (zipFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    while (!zipFile.atEnd())
                        m_description = "\n" + zipFile.readAll();
            }
        }

        pk3.close();
    }

    if (!m_description.isEmpty())
    {
        if (!m_isMarkdown)
            m_description = "<h1>" + m_name + "</h1>\n\n" + m_description;
    }
}

QString Pk3Handler::name() const
{
    return m_name;
}

QString Pk3Handler::description() const
{
    return m_description;
}

bool Pk3Handler::isMarkdown() const
{
    return m_isMarkdown;
}
